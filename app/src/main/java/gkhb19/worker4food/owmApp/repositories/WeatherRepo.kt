package gkhb19.worker4food.owmApp.repositories

import android.content.ContentValues
import android.content.Context
import androidx.core.database.sqlite.transaction
import gkhb19.worker4food.owmApp.data.*
import gkhb19.worker4food.owmApp.db.*
import gkhb19.worker4food.owmApp.extensions.*
import gkhb19.worker4food.owmApp.services.OwmApiService
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class WeatherRepo(private val ctx: Context) {
    private val forecastTbl = WeatherDbHelper.Companion.ForecastTbl
    private val cityTbl = WeatherDbHelper.Companion.CityTbl

    private val api by lazy { OwmApiService(ctx) }

    fun readDBCacheOrNull(): OwmResponse? = WeatherDbHelper(ctx).readableDatabase!!.use { db ->
        db.query(
            cityTbl.TABLE_NAME,
            null, // columns
            null, // selection
            null, // selectionArgs
            null, // groupBy
            null, // having
            "${cityTbl.LAST_UPDATE} DESC", // orderBy
            "1" // limit
        ).use {
            if (it.moveToFirst())
                City(
                    id = it.get(cityTbl.ID),
                    name = it.get(cityTbl.NAME),
                    country = it.get(cityTbl.COUNTRY),
                    coord = Coord(
                        lat = it.get(cityTbl.COORD_LAT),
                        lon = it.get(cityTbl.COORD_LON)
                    )
                )
            else null
        }?.let { city ->
            db.query(
                forecastTbl.TABLE_NAME,
                null, // columns
                "${forecastTbl.CITY} = ${city.id}", // selection
                null, // selectionArgs
                null, // groupBy
                null, // having
                null // orderBy
            ).use { cursor ->
                val res = mutableListOf<Forecast>()

                while (cursor.moveToNext()) {
                    Forecast(
                        dt = cursor.get<String>(forecastTbl.DT).let(iso8601::parse)!!,
                        sensors = Sensors(
                            temp = cursor.get(forecastTbl.SENSORS_TEMP),
                            pressure = cursor.get(forecastTbl.SENSORS_PRESSURE),
                            humidity = cursor.get(forecastTbl.SENSORS_HUMIDITY)
                        ),
                        weather = Weather(
                            main = cursor.get(forecastTbl.WEATHER_MAIN),
                            description = cursor.get(forecastTbl.WEATHER_DESCRIPTION),
                            icon = cursor.get(forecastTbl.WEATHER_ICON)
                        ),
                        wind = Wind(
                            speed = cursor.get(forecastTbl.WIND_SPEED),
                            deg = cursor.get(forecastTbl.WIND_DEG)
                        ),
                        clouds = cursor.getOrNull<Long>(forecastTbl.CLOUDS_ALL)
                            ?.let(::Clouds),
                        rain = cursor.getOrNull<Double>(forecastTbl.RAIN_3H)
                            ?.let(::PrecipitationLevel),
                        snow = cursor.getOrNull<Double>(forecastTbl.SNOW_3H)
                            ?.let(::PrecipitationLevel)
                    ).also(res::add)
                }

                OwmResponse(
                    cod = RESPONSE_CODE_FROM_DB,
                    city = city,
                    list = res,
                    cnt = res.size.toLong()
                ).takeIf { res.size > 0 }
            }
        }
    }

    private fun writeDBCache(resp: OwmResponse) = writeDBCache(resp.city, resp.list)

    private fun writeDBCache(city: City, forecasts: List<Forecast>): Unit =
        WeatherDbHelper(ctx).writableDatabase!!.use { db ->
            db.transaction(exclusive = false) {
                delete(cityTbl.TABLE_NAME, null, null)
                delete(forecastTbl.TABLE_NAME, null, null)

                ContentValues().apply {
                    put(cityTbl.ID, city.id)
                    put(cityTbl.NAME, city.name)
                    put(cityTbl.COUNTRY, city.country)
                    put(cityTbl.COORD_LAT, city.coord.lat)
                    put(cityTbl.COORD_LON, city.coord.lon)
                }.also {
                    insertOrThrow(cityTbl.TABLE_NAME, null, it)
                }

                forecasts.forEach {
                    ContentValues().apply {
                        put(forecastTbl.DT, iso8601.format(it.dt))

                        put(forecastTbl.SENSORS_TEMP, it.sensors.temp)
                        put(forecastTbl.SENSORS_HUMIDITY, it.sensors.humidity)
                        put(forecastTbl.SENSORS_PRESSURE, it.sensors.pressure)

                        put(forecastTbl.WEATHER_MAIN, it.weather.main)
                        put(forecastTbl.WEATHER_DESCRIPTION, it.weather.description)
                        put(forecastTbl.WEATHER_ICON, it.weather.icon)

                        put(forecastTbl.WIND_SPEED, it.wind.speed)
                        put(forecastTbl.WIND_DEG, it.wind.deg)

                        put(forecastTbl.CLOUDS_ALL, it.clouds?.all)

                        put(forecastTbl.RAIN_3H, it.rain?.the3h)
                        put(forecastTbl.SNOW_3H, it.snow?.the3h)

                        put(forecastTbl.CITY, city.id)
                    }.also {
                        insertOrThrow(forecastTbl.TABLE_NAME, null, it)
                    }
                }
            }
        }

    private suspend fun getForecast(city: String, units: String): OwmResponse =
        api.client.getForecast(city, units).also(::writeDBCache)

    suspend fun getForecastOrNull(city: String, units: String): OwmResponse? =
        try {
            getForecast(city, units)
        } catch (_: Exception) {
            null
        }

    companion object {
        val iso8601 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.US)
        const val RESPONSE_CODE_FROM_DB = "42"
    }
}
