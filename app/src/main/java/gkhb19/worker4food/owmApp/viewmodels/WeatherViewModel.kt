package gkhb19.worker4food.owmApp.viewmodels

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities.*
import androidx.lifecycle.*
import gkhb19.worker4food.owmApp.StatusOf
import gkhb19.worker4food.owmApp.data.*
import gkhb19.worker4food.owmApp.repositories.WeatherRepo
import gkhb19.worker4food.owmApp.settings.*
import gkhb19.worker4food.owmApp.R
import khronos.*
import kotlinx.coroutines.launch
import java.lang.Exception
import java.util.*

class WeatherViewModel(app: Application) : AndroidViewModel(app) {

    init {
        updateWeatherInfo()
    }

    private var _prefs = readSettings()

    private val repo by lazy { WeatherRepo(getApplication()) }

    private val _currentIx = MutableLiveData<Int>()
    private val _apiResponse = MutableLiveData<StatusOf<OwmResponse>>()

    private val response: LiveData<OwmResponse> = Transformations.map(_apiResponse) {
        if (it is StatusOf.Ok) it.result else null
    }

    val result: LiveData<StatusOf<OwmResponse>> = _apiResponse

    val error: LiveData<String> = Transformations.map(_apiResponse) {
        if (it is StatusOf.Error) it.msg else null
    }

    val weatherInfo: LiveData<List<DailyForecast>> = Transformations.map(response) {
        it?.let { owmResp ->
            val seedFn = { _: Date, e: Forecast ->
                AggregatedForecast(e.sensors, e.sensors) to listOf<Forecast>()
            }

            owmResp.list
                .groupingBy { x -> x.dt.beginningOfDay }
                .fold(seedFn) { _, (acc, forecasts), e ->
                    acc.copyWith(e.sensors) to (forecasts + e)
                }
                .map { (date, info) ->
                    DailyForecast(date, info.first, info.second)
                }
        }
    }

    val selected: LiveData<DailyForecast> = Transformations.map(_currentIx) {
        it?.let {
            weatherInfo.value?.get(it)
        }
    }

    val temperatureUnits: String
        get() = getUnits("temp")

    val speedUnits: String
        get() = getUnits("speed")

    private fun readSettings() = with(getApplication<Application>()) {
        AppSettings.fromPrefs(
            getSharedPreferences(prefsFileName, Context.MODE_PRIVATE),
            resources
        )
    }

    fun updateWeatherInfo(settings: AppSettings = readSettings()) = viewModelScope.launch {
        _apiResponse.postValue(StatusOf.Loading)

        _prefs = settings

        try {
            var r: OwmResponse? = null

            if (isNetworkOk())
                r = repo.getForecastOrNull(_prefs.city, _prefs.units)

            (r ?: repo.readDBCacheOrNull())
                ?.let { StatusOf.Ok(it) }
                ?: getApplication<Application>().resources.getString(R.string.final_error).let {
                    throw Exception(it)
                }

        } catch (e: Exception) {
            StatusOf.Error<OwmResponse>(e.message!!)
        }.also(_apiResponse::postValue)
    }

    fun selectByIndex(ix: Int) {
        if (ix != _currentIx.value)
            _currentIx.postValue(ix)
    }

    fun clearSelection() = _currentIx.postValue(null)

    private fun isNetworkOk(): Boolean {
        val connMgr = getApplication<Application>()
            .getSystemService(ConnectivityManager::class.java)

        return connMgr
            ?.activeNetwork
            ?.let(connMgr::getNetworkCapabilities)
            ?.let { netCaps ->
                setOf(TRANSPORT_WIFI, TRANSPORT_CELLULAR, TRANSPORT_ETHERNET)
                    .any(netCaps::hasTransport)
            }
            ?: false
    }

    private fun getUnits(prefix: String, units: String = _prefs.units): String {
        val app = getApplication<Application>()

        return app.resources.getString(
            app.resources.getIdentifier("${prefix}_${units}", "string", app.packageName)
        )
    }
}
