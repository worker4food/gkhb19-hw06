package gkhb19.worker4food.owmApp.data

import kotlinx.serialization.*
import kotlinx.serialization.internal.StringDescriptor
import java.util.*

@Serializable
data class OwmResponse(
    val cod: String,
    val cnt: Long,
    val list: List<Forecast>,
    val city: City
)

@Serializable
data class City(
    val id: Long,
    val name: String,
    val coord: Coord,
    val country: String
)

@Serializable
data class Coord(
    val lat: Double,
    val lon: Double
)

@Serializable
data class Forecast(
    @Serializable(with = UnixEpochSerializer::class)
    val dt: Date,

    @SerialName("main")
    val sensors: Sensors,

    @Serializable(with = FirstWeather::class)
    val weather: Weather,

    val wind: Wind,
    val clouds: Clouds? = null,
    val rain: PrecipitationLevel? = null,
    val snow: PrecipitationLevel? = null
)

@Serializable
data class Clouds(val all: Long)

@Serializable
data class Sensors(
    val temp: Double,
    val pressure: Long,
    val humidity: Long
)

@Serializable
data class PrecipitationLevel(
    @SerialName("3h")
    val the3h: Double
)

@Serializable
data class Weather(
    val main: String,
    val description: String,
    val icon: String
)

@Serializable
data class Wind(
    val speed: Double,
    val deg: Long
)

class UnixEpochSerializer : KSerializer<Date> {
    override val descriptor: SerialDescriptor
        get() = StringDescriptor.withName("unix timestamp (de)serializer")

    override fun deserialize(decoder: Decoder): Date = decoder.decodeLong().times(1000L).let(::Date)

    override fun serialize(encoder: Encoder, obj: Date) = encoder.encodeLong(obj.time / 1000L)
}

open class FirstOf<T>(private val listSerializer: KSerializer<List<T>>) : KSerializer<T> {
    override val descriptor: SerialDescriptor
        get() = StringDescriptor.withName("first elements of List<T> (de)serializer")

    override fun deserialize(decoder: Decoder): T = decoder.decode(listSerializer).first()

    override fun serialize(encoder: Encoder, obj: T) = encoder.encode(listSerializer, listOf(obj))
}

class FirstWeather : FirstOf<Weather>(Weather.serializer().list)

infix fun Sensors.takeMin(new: Sensors) =
    copy(
        temp = minOf(temp, new.temp),
        pressure = minOf(pressure, new.pressure),
        humidity = minOf(humidity, new.humidity)
    )

infix fun Sensors.takeMax(new: Sensors) =
    copy(
        temp = maxOf(temp, new.temp),
        pressure = maxOf(pressure, new.pressure),
        humidity = maxOf(humidity, new.humidity)
    )

//fun <T: Comparable<T>> Sensors.takeWith(new: Sensors, selector: (T, T) -> T) = copy(
//    temp = selector(temp, new.temp)
//)

fun AggregatedForecast.copyWith(sensors: Sensors) =
    AggregatedForecast(
        sensors takeMin from,
        sensors takeMax to
    )

data class AggregatedForecast(val from: Sensors, val to: Sensors)

data class DailyForecast(
    val date: Date,
    val day: AggregatedForecast,
    val hours: List<Forecast>
)

fun DailyForecast.precDescription() = mutableListOf<String>().apply {
        if (hours.any { it.rain != null })
            add("Rain")

        if (hours.any { it.snow != null })
            add("Snow")
    }.run { joinToString() }
