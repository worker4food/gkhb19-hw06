package gkhb19.worker4food.owmApp.extensions

import android.database.Cursor
import androidx.core.database.getDoubleOrNull
import androidx.core.database.getIntOrNull
import androidx.core.database.getLongOrNull
import androidx.core.database.getStringOrNull

inline fun <reified T> Cursor.get(column: String): T =
    when (T::class) {
        String::class -> getString(getColumnIndexOrThrow(column))
        Long::class -> getLong(getColumnIndexOrThrow(column))
        Int::class -> getInt(getColumnIndexOrThrow(column))
        Double::class -> getDouble(getColumnIndexOrThrow(column))
        else -> TODO(T::class.toString())
    } as T

inline fun <reified T> Cursor.getOrNull(column: String): T? =
    when (T::class) {
        String::class -> getStringOrNull(getColumnIndexOrThrow(column))
        Long::class -> getLongOrNull(getColumnIndexOrThrow(column))
        Int::class -> getIntOrNull(getColumnIndexOrThrow(column))
        Double::class -> getDoubleOrNull(getColumnIndexOrThrow(column))
        else -> TODO(T::class.toString())
    } as T?
