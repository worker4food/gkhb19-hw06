package gkhb19.worker4food.owmApp.settings

import android.content.SharedPreferences
import android.content.res.Resources
import androidx.core.content.edit
import gkhb19.worker4food.owmApp.R

const val prefsFileName = "app.conf"

const val keyUnits = "conf_units"
const val keyCity = "conf_city"

data class AppSettings(val city: String, val units: String) {
    companion object {
        fun fromPrefs(prefs: SharedPreferences, r: Resources) = with(prefs) {
            AppSettings(
                getString(keyCity, null) ?: r.getString(R.string.default_city),
                getString(keyUnits, null) ?: r.getStringArray(R.array.valid_units)[0]
            )
        }
    }
}


fun AppSettings.toPrefs(prefs: SharedPreferences) = prefs.edit {
    putString(keyCity, city)
    putString(keyUnits, units)
}
