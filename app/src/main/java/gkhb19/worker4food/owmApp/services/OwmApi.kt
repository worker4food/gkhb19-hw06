package gkhb19.worker4food.owmApp.services

import gkhb19.worker4food.owmApp.data.OwmResponse
import retrofit2.http.*

interface OwmApi {
    @GET("forecast")
    suspend fun getForecast(
        @Query("q") city: String,
        @Query("units") units: String = "metric",
        @Query("lang") lang: String = "en"
    ) : OwmResponse
}

