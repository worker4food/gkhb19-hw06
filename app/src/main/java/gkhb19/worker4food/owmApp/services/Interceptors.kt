package gkhb19.worker4food.owmApp.services

import kotlinx.serialization.json.*
import okhttp3.*
import okhttp3.ResponseBody.Companion.toResponseBody

class Interceptors {
    companion object {

        fun auth(key: String) = Interceptor { chain ->
            val newUrl = chain.request()
                .url
                .newBuilder()
                .addQueryParameter("appid", key)
                .build()

            chain.request().newBuilder().url(newUrl).build().let(chain::proceed)
        }

        fun prettier(ourMediaType: MediaType) = Interceptor { chain ->
            val response = chain.proceed(chain.request())

            if (ourMediaType.subtype == response.body?.contentType()?.subtype) {
                val responseStr = response.body?.string() ?: ""
                val newBody = with(Json.indented) {
                    val jsObj = parse(JsonObject.serializer(), responseStr)

                    stringify(JsonObject.serializer(), jsObj).toResponseBody(ourMediaType)
                }

                response.newBuilder().body(newBody).build()
            } else
                response
        }
    }
}
