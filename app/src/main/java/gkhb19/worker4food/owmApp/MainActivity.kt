package gkhb19.worker4food.owmApp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import androidx.activity.*
import androidx.navigation.fragment.*
import androidx.navigation.ui.*
import gkhb19.worker4food.owmApp.extensions.isPortrait
import gkhb19.worker4food.owmApp.viewmodels.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val model by viewModels<WeatherViewModel>()
    private val appBarConfig = AppBarConfiguration.Builder(
        R.id.aggregatedForecastFragment,
        R.id.loadingFragment,
        R.id.errorFragment
    ).build()

    private val navController
        get() = mainNavHost.findNavController()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        setupActionBarWithNavController(navController, appBarConfig)

        val statusDests: Set<Int> =
            appBarConfig.topLevelDestinations - navController.graph.startDestination

        model.result.observe(this) { status ->
            when (status) {
                is StatusOf.Loading -> navController.navigate(R.id.loadingFragment)

                is StatusOf.Error -> navController.navigate(R.id.errorFragment)

                is StatusOf.Ok -> navController.currentDestination?.id?.let {
                    if (statusDests.contains(it))
                        navController.popBackStack(
                            navController.graph.startDestination,
                            false
                        )
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.secondary, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        item.onNavDestinationSelected(navController) || super.onOptionsItemSelected(item)

    override fun onSupportNavigateUp(): Boolean {
        onBackOrUpPressed()
        return navController.navigateUp(appBarConfig)
    }

    override fun onBackPressed() {
        onBackOrUpPressed()
        return super.onBackPressed()
    }

    private fun onBackOrUpPressed() {
        when (navController.currentDestination?.id) {
            R.id.weatherDetailsFragment -> if (isPortrait) model.clearSelection()
            R.id.loadingFragment, R.id.errorFragment -> finish()
        }
    }
}
