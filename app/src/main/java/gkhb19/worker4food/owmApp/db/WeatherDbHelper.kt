package gkhb19.worker4food.owmApp.db

import android.content.Context
import android.database.sqlite.*
import android.provider.BaseColumns

class WeatherDbHelper(ctx: Context) : SQLiteOpenHelper(ctx, DB_NAME, null, DB_VER) {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(CityTbl.SQL_CREATE)
        db.execSQL(ForecastTbl.SQL_CREATE)
    }

    override fun onUpgrade(db: SQLiteDatabase, from: Int, to: Int) {
        db.execSQL(ForecastTbl.SQL_DROP)
        db.execSQL(CityTbl.SQL_DROP)

        onCreate(db)
    }

    companion object {
        const val DB_NAME = "forecast.sqlite3"
        const val DB_VER = 1

        object ForecastTbl: BaseColumns {
            const val TABLE_NAME = "forecast"

            const val CITY = "city_id"
            const val DT = "dt"

            // Sensors
            const val SENSORS_TEMP = "sensors_temp"
            const val SENSORS_HUMIDITY = "sensors_humidity"
            const val SENSORS_PRESSURE = "sensors_pressure"

            // Weather
            const val WEATHER_MAIN = "weather_main"
            const val WEATHER_DESCRIPTION = "weather_description"
            const val WEATHER_ICON = "weather_icon"

            // Wind
            const val WIND_SPEED = "wind_speed"
            const val WIND_DEG = "wind_deg"

            // Clouds
            const val CLOUDS_ALL = "clouds_all"

            // PrecipitationLevel
            const val RAIN_3H = "rain_3h"
            const val SNOW_3H = "snow_3h"

            val SQL_CREATE =
                "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
                        listOf(
                            "$DT TEXT NOT NULL",
                            "$SENSORS_TEMP DOUBLE NOT NULL",
                            "$SENSORS_HUMIDITY LONG NOT NULL",
                            "$SENSORS_PRESSURE LONG NOT NULL",
                            "$WEATHER_MAIN TEXT NOT NULL",
                            "$WEATHER_DESCRIPTION TEXT NOT NULL",
                            "$WEATHER_ICON TEXT NOT NULL",
                            "$WIND_SPEED DOUBLE NOT NULL",
                            "$WIND_DEG LONG NOT NULL",
                            "$CLOUDS_ALL LONG DEFAULT NULL",
                            "$RAIN_3H DOUBLE DEFAULT NULL",
                            "$SNOW_3H DOUBLE DEFAULT NULL",
                            "$CITY LONG NOT NULL",
                            "FOREIGN KEY($CITY) REFERENCES ${CityTbl.TABLE_NAME}(${CityTbl.ID})"
                        ).joinToString() +
                        ")"

            const val SQL_DROP =
                "DROP TABLE IF EXISTS $TABLE_NAME"
        }

        object CityTbl: BaseColumns {
            const val TABLE_NAME = "city"

            val ID
                get() = BaseColumns._ID

            const val NAME = "name"
            const val COUNTRY = "country"

            // Coord
            const val COORD_LON = "coord_lon"
            const val COORD_LAT = "coord_lat"

            const val LAST_UPDATE = "last_update"

            val SQL_CREATE =
                "CREATE TABLE IF NOT EXISTS $TABLE_NAME(" +
                        "$ID LONG PRIMARY KEY, " +
                        "$NAME TEXT NOT NULL, " +
                        "$COUNTRY TEXT NOT NULL, " +
                        "$COORD_LAT DOUBLE NOT NULL, " +
                        "$COORD_LON DOUBLE NOT NULL, " +
                        "$LAST_UPDATE TIMESTAMP DEFAULT CURRENT_TIMESTAMP" +
                        ")"

            const val SQL_DROP =
                "DROP TABLE IF EXISTS $TABLE_NAME"
        }
    }
}
