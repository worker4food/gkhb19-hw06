package gkhb19.worker4food.owmApp

sealed class StatusOf<out T> {
    object Loading: StatusOf<Nothing>()
    data class Error<T>(val msg: String): StatusOf<T>()
    data class Ok<T>(val result: T): StatusOf<T>()
}
