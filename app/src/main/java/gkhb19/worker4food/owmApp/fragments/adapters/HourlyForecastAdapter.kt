package gkhb19.worker4food.owmApp.fragments.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.*
import androidx.core.net.toUri
import coil.api.load
import gkhb19.worker4food.owmApp.R
import gkhb19.worker4food.owmApp.data.*
import gkhb19.worker4food.owmApp.viewmodels.WeatherViewModel
import kotlinx.android.synthetic.main.fragment_hourlyforecast_list_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class HourlyForecastAdapter(private val model: WeatherViewModel) :
    RecyclerView.Adapter<HourlyForecastAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_hourlyforecast_list_item, parent, false)
            .let(::ViewHolder)


    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(
        model.selected.value!!.hours[position]
    )

    override fun getItemCount(): Int = model.selected.value?.hours?.size ?: 0

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(forecast: Forecast) {

            val ctx = itemView.context

            with(itemView) {
                time.text = SimpleDateFormat("HH:mm", Locale.US)
                    .format(forecast.dt)

                description.text = forecast.weather.description

                icon.load("file:///android_asset/${forecast.weather.icon}@2x.png".toUri())

                exactTemp.text = ctx.getString(
                    R.string.temp_format,
                    forecast.sensors.temp,
                    model.temperatureUnits
                )

                exactHum.text = ctx.getString(
                    R.string.humidity_format,
                    forecast.sensors.humidity
                )

                exactPressure.text = ctx.getString(
                    R.string.pressure_format,
                    forecast.sensors.pressure
                )

                exactRain.visibility = if (forecast.rain != null) View.VISIBLE else View.GONE
                exactRain.text = ctx.getString(
                    R.string.precipitation_format,
                    "rain",
                    forecast.rain?.the3h
                )

                exactSnow.visibility = if (forecast.snow != null) View.VISIBLE else View.GONE
                exactSnow.text = ctx.getString(
                    R.string.precipitation_format,
                    "snow",
                    forecast.snow?.the3h
                )

                exactClouds.text = ctx.getString(
                    R.string.clouds_format,
                    forecast.clouds?.all
                )

                exactWindIcon.rotation = forecast.wind.deg.toFloat()

                exactWindSpeed.text = ctx.getString(
                    R.string.wind_speed_format,
                    forecast.wind.speed,
                    model.speedUnits
                )
            }
        }
    }
}
