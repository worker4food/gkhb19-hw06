package gkhb19.worker4food.owmApp.fragments

import android.os.Bundle
import androidx.fragment.app.*
import android.view.*
import androidx.navigation.fragment.findNavController
import gkhb19.worker4food.owmApp.R
import gkhb19.worker4food.owmApp.fragments.adapters.*
import gkhb19.worker4food.owmApp.extensions.*
import gkhb19.worker4food.owmApp.viewmodels.*
import kotlinx.android.synthetic.main.fragment_aggregatedforecast_list.*

class AggregatedForecastFragment : Fragment() {

    private val model by activityViewModels<WeatherViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_aggregatedforecast_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        aggregatedList.adapter = AggregatedForecastAdapter(model)

        model.weatherInfo.observe(viewLifecycleOwner) {
            aggregatedList.adapter?.notifyDataSetChanged()
        }

        if (isPortrait) model.selected.observe(viewLifecycleOwner) {
            if (it != null)
                findNavController().navigate(R.id.weatherDetailsFragment)
        }
    }
}
