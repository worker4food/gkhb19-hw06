package gkhb19.worker4food.owmApp.fragments.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.*
import gkhb19.worker4food.owmApp.R
import gkhb19.worker4food.owmApp.data.*
import gkhb19.worker4food.owmApp.viewmodels.WeatherViewModel
import java.text.SimpleDateFormat
import kotlinx.android.synthetic.main.fragment_aggregatedforecast_list_item.view.*
import java.util.*

class AggregatedForecastAdapter(private val model: WeatherViewModel) :
    RecyclerView.Adapter<AggregatedForecastAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_aggregatedforecast_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(model.weatherInfo.value!![position])

    override fun getItemCount(): Int = model.weatherInfo.value?.size ?: 0

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(_v: View?) = model.selectByIndex(adapterPosition)

        fun bind(forecast: DailyForecast) {
            val ctx = itemView.context!!

            itemView.weekDay.text = SimpleDateFormat("EEE", Locale.US)
                .format(forecast.date)

            itemView.date.text = SimpleDateFormat("dd/MM", Locale.US)
                .format(forecast.date)

            itemView.temperature.text = ctx.getString(
                R.string.temp_range_format,
                forecast.day.from.temp,
                forecast.day.to.temp,
                model.temperatureUnits
            )

            val pressure = setOf(forecast.day.from.pressure, forecast.day.to.pressure)
                .joinToString(" - ")
            itemView.pressure.text = ctx.getString(R.string.pressure_format_string, pressure)

            val humidity = setOf(forecast.day.from.humidity, forecast.day.to.humidity)
                .joinToString(" - ")
            itemView.humidity.text = ctx.getString(R.string.humidity_format_string, humidity)

            val prec = forecast.precDescription()
            itemView.precipitations.visibility = if (prec.isNotBlank()) View.VISIBLE else View.GONE
            itemView.precipitations.text = prec
        }
    }
}
