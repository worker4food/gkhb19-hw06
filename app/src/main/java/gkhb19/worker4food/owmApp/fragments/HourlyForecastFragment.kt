package gkhb19.worker4food.owmApp.fragments

import android.os.Bundle
import androidx.fragment.app.*
import android.view.*
import androidx.navigation.fragment.findNavController
import gkhb19.worker4food.owmApp.R
import gkhb19.worker4food.owmApp.extensions.*
import gkhb19.worker4food.owmApp.fragments.adapters.*
import gkhb19.worker4food.owmApp.viewmodels.WeatherViewModel
import kotlinx.android.synthetic.main.fragment_hourlyforecast_list.*
import java.text.SimpleDateFormat
import java.util.*

class HourlyForecastFragment : Fragment() {

    private val model by activityViewModels<WeatherViewModel>()
    private val adapter by lazy { HourlyForecastAdapter(model) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_hourlyforecast_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        hourlyList.adapter = adapter

        model.selected.observe(viewLifecycleOwner) { forecast ->
            hourlyList.adapter?.notifyDataSetChanged()
            hourlyList.scrollToPosition(0)

            if (forecast != null)
                hourlyDayInfo.text = SimpleDateFormat("EEE, MMM dd", Locale.US)
                    .format(forecast.date)
        }

        if (isLandscape)
            findNavController().navigateUp()
    }
}
