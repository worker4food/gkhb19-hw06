package gkhb19.worker4food.owmApp.fragments

import android.os.Bundle
import android.view.*
import android.widget.ArrayAdapter
import androidx.fragment.app.*
import androidx.navigation.fragment.findNavController
import gkhb19.worker4food.owmApp.R
import gkhb19.worker4food.owmApp.settings.*
import gkhb19.worker4food.owmApp.extensions.*
import gkhb19.worker4food.owmApp.viewmodels.WeatherViewModel
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment : Fragment() {

    private val model by activityViewModels<WeatherViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) = Unit

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_settings, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val conf = readSettings()

        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.cities,
            android.R.layout.simple_spinner_item
        ).also {
            citySelector.setAdapter(it)
            citySelector.setText(conf.city)
            citySelector.threshold = 1
        }

        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.valid_units,
            android.R.layout.simple_spinner_item
        ).also {
            val pos = maxOf(it.getPosition(conf.units), 0)

            unitsSelector.adapter = it
            unitsSelector.setSelection(pos)
        }

        btnSave.setOnClickListener {
            val newSettings = AppSettings(
                "${citySelector.text}",
                "${unitsSelector.selectedItem}"
            )

            saveSettings(newSettings)

            model.clearSelection()
            model.updateWeatherInfo(newSettings)

            findNavController().apply {
                popBackStack(graph.startDestination, false)
            }
        }
    }
}
